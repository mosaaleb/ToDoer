## :memo: :heavy_check_mark: ToDoer

> A javascript powered todo app.

#### Installation:
```
git clone git@github.com:mosaaleb/ToDoer.git
cd ToDoer
npm install

# To build the app for production, run the following command:
npm run build-prod
```

#### Description
The application built using the **MVC** Architecture and **Observer** design pattern.

![Screenshot from 2019-10-14 10-03-46](https://user-images.githubusercontent.com/4246637/66750078-cc7b2c80-ee8b-11e9-8597-5fbba96d8634.png)

#### Tech Used: 
- [webpack](https://webpack.js.org/)
- [handlebars](https://handlebarsjs.com/)
- [tailwindcss](https://tailwindcss.com/)

#### Contributers:
- [Darshan](https://github.com/juzQrios)
- [Muhammad](https://github.com/mosaaleb)

