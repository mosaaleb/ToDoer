module.exports = {
  theme: {
    extend: {
      fontFamily: {
        'body': ['Work Sans', 'sans-serif'],
      },
      boxShadow: {
        // box-shadow: <inset*> <offset-x> <offset-y> <blur-radius*> <spread-radius*> <color*>;
        inner: 'inset -2px 0 10px 0 rgba(0, 0, 0, 0.06)',
      },
      colors: {
        silverGray: '#c5d0eb'
        // <!-- #DBE4FA #DBE4FA #58A3F4 #B3BFD7 #284AF6 #2637AA #E7EBF7-->
      }
    }
  },
  variants: {},
  plugins: []
};
